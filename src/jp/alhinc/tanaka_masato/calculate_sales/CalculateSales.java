package jp.alhinc.tanaka_masato.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String[] args) {

		HashMap<String, String> branchMap = new HashMap<>();
		HashMap<String, Long> salesMap = new HashMap<>();
		BufferedReader br = null;

		try {
			try {
				File branchFile = new File(args[0], "branch.lst"); //args[0]は「実行の構成」→「(x)=引数」から定義
				//支店定義ファイルが存在しない場合
				if (!branchFile.exists()) {
					System.out.println("支店定義ファイルが存在しません");
					return;
				}

				br = new BufferedReader(new FileReader(branchFile));

				String branchList;
				while ((branchList = br.readLine()) != null) { //1行ごとにテキストを読み込む(改行は含まない)
					String[] branchParts = branchList.split(","); //文字列（ここでは「,」）で分割させ、配列の要素として格納

					//支店定義ファイルが不正の場合
					if (branchParts.length != 2 || !branchParts[0].matches("^\\d{3}$")) { //配列の要素が2つorコード名が3桁数字じゃない
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					branchMap.put(branchParts[0], branchParts[1]);

					//salesMap(code,〇〇〇)の初期化
					salesMap.put(branchParts[0], 0L);
				}
			} finally {
				if (br != null) {
					br.close();
				}
			}

			//売上ファイルを読み込む
			try {
				//指定した条件のファイルのみをフィルタリングする
				FilenameFilter filter = new FilenameFilter() {
					public boolean accept(File dir, String fileName) { //下記で指定した文字列が存在した場合
						return fileName.matches("^\\d{8}.rcd$") && new File(dir, fileName).isFile(); //「{8桁数字}.rcd」を指定する
					}
				};

				File[] matchFile = new File(args[0]).listFiles(filter); //ディレクトリ内のフィルタリングされたファイルを配列に格納

				Arrays.sort(matchFile); //配列を昇順に並べ替える

				//ファイル名が連番になっていない場合
				String small = matchFile[0].getName();
				String[] smallParts = small.split("\\.");
				int smallNumber = Integer.parseInt(smallParts[0]);

				String large = matchFile[matchFile.length - 1].getName();
				String[] largeParts = large.split("\\.");
				int largeNumber = Integer.parseInt(largeParts[0]);

				if (matchFile.length - 1 != largeNumber - smallNumber) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

				for (File salesFile : matchFile) { //取得しているファイルを1つずつ入れていく
					br = new BufferedReader(new FileReader(salesFile));
					String code = br.readLine(); //1行目の文字列を代入
					Long sales = Long.parseLong(br.readLine()); //2行目の文字列を代入

					//支店に該当がなかった場合
					if (!branchMap.containsKey(code)) { //Mapのキーがないもしくはコードが3桁じゃない場合
						System.out.println(salesFile.getName() + "の支店コードが不正です");
						return;
					}

					//売上ファイルの中身が3行以上ある場合
					if (br.readLine() != null) {
						System.out.println(salesFile.getName() + "のフォーマットが不正です");
						return;
					}

					Long sum = sales + salesMap.get(code);
					//合計金額が10桁を超えた場合
					if (sum.toString().length() > 10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}

					salesMap.put(code, sum);
				}
			} finally {
				if (br != null) {
					br.close();
				}
			}

			//集計データの出力
			BufferedWriter bw = null;
			try {
				File file = new File(args[0], "branch.out");
				bw = new BufferedWriter(new FileWriter(file));

				//キーと値のペアをすべて取得し、1つずついれていく
				for (Entry<String, String> entry : branchMap.entrySet()) {
					String code = entry.getKey(); //キーを代入
					String branchName = entry.getValue(); //値を代入
					bw.write(code + "," + branchName + "," + salesMap.get(code));
					bw.newLine(); //改行する
				}
			} finally {
				if (bw != null) {
					bw.close();
				}
			}
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}
